import React, { useState , useEffect } from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Movie from "@mui/icons-material/Movie";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";

const defaultTheme = createTheme();

export default function SignIn() {
	// const [submitLogin, { data, loading, error }] = useMutation(LOGIN);

	useEffect( () => {
		const dataToken = localStorage.getItem("access_token", JSON.stringify(''));
		if(dataToken !== '""')
		{
			window.location = "/dashboard";
			return;
		}

	}, [])

	const handleSubmit = async (event) => {
		event.preventDefault();
		const formData = new FormData(event.currentTarget);
		// await submitLogin({ variables: {
		// 	accountNumber: formData.get("accountNumber"),
		// 	password: formData.get("password"),
		// }});
		// if (data) {
		// 	const {
		// 		login: { success, user },
		// 	} = data;
		// 	if (success) {
				localStorage.setItem("user", JSON.stringify(''));
				localStorage.setItem("access_token", JSON.stringify('access_token_test'));
				window.location = "/dashboard";
		// 	}
		// } else {
		// 	alert("Login failed");
		// }
	};

	return (
		<ThemeProvider theme={defaultTheme}>
			<Container component="main" maxWidth="xs">
				<CssBaseline />
				<Box
					sx={{
						marginTop: 8,
						display: "flex",
						flexDirection: "column",
						alignItems: "center",
					}}
				>
					<Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
						<Movie />
					</Avatar>
					<Typography component="h1" variant="h5">
						Đặt vé xem phim Online
					</Typography>
					<Box
						component="form"
						onSubmit={handleSubmit}
						noValidate
						sx={{ mt: 1 }}
					>
						<TextField
							margin="normal"
							required
							fullWidth
							id="accountNumber"
							label="Tên tài khoản"
							name="accountNumber"
							autoComplete="accountNumber"
							autoFocus
						/>
						<TextField
							margin="normal"
							required
							fullWidth
							name="password"
							label="Mật khẩu"
							type="password"
							id="password"
							autoComplete="current-password"
						/>
						<Button
							type="submit"
							fullWidth
							variant="contained"
							sx={{ mt: 3, mb: 2 }}
						>
							Đăng Nhập
						</Button>
					</Box>
				</Box>
			</Container>
		</ThemeProvider>
	);
}
